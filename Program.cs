﻿using System;
using System.Collections.Generic;

namespace Game
{
    class Program
    {
        static void Main(string[] args)
        {
            Program a = new Program();
            a.play();
        }

        public void play()
        {
            Console.WriteLine("Do you want to play?");
            Console.WriteLine("yes/no");

            string toPlayAgain = Console.ReadLine();

            int playerCount = 0;
            int computerCount = 0;

            while (toPlayAgain != "no")
            {

                Console.WriteLine("Pick Rock, Paper or Scissors?");
                string playerMove = Console.ReadLine();

                if (playerMove == "no")
                {
                    break;
                }

                var forms = new List<string> { "Rock", "Paper", "Scissors" };
                Random random = new Random();
                int index = random.Next(forms.Count);

                var form = forms[index];

                Console.WriteLine("Computer Move: " + form);

                if (playerMove.ToLower() == "rock" && form.ToLower() == "rock")
                {

                    Console.WriteLine("Draw. Try Again.");
                    Console.WriteLine("Player: " + playerMove);
                    Console.WriteLine("Computer: " + form);
                    computerCount = computerCount + 0;
                    playerCount = playerCount + 0;
                    Console.WriteLine("Computer Score: " + computerCount);
                    Console.WriteLine("Player Score: " + playerCount);

                }
                else if (playerMove.ToLower() == "rock" && form.ToLower() == "paper")
                {

                    Console.WriteLine("Computer Wins.");
                    Console.WriteLine("Player: " + playerMove);
                    Console.WriteLine("Computer: " + form);
                    computerCount = computerCount + 1;
                    playerCount = playerCount + 0;
                    Console.WriteLine("Computer Score: " + computerCount);
                    Console.WriteLine("Player Score: " + playerCount);


                }
                else if (playerMove.ToLower() == "rock" && form.ToLower() == "scissors")
                {

                    Console.WriteLine("Player Wins.");
                    Console.WriteLine("Player: " + playerMove);
                    Console.WriteLine("Computer: " + form);
                    computerCount = computerCount + 0;
                    playerCount = playerCount + 1;
                    Console.WriteLine("Computer Score: " + computerCount);
                    Console.WriteLine("Player Score: " + playerCount);

                }
                else if (playerMove.ToLower() == "paper" && form.ToLower() == "rock")
                {

                    Console.WriteLine("Player Wins.");
                    Console.WriteLine("Player: " + playerMove);
                    Console.WriteLine("Computer: " + form);
                    computerCount = computerCount + 0;
                    playerCount = playerCount + 1;
                    Console.WriteLine("Computer Score: " + computerCount);
                    Console.WriteLine("Player Score: " + playerCount);

                }
                else if (playerMove.ToLower() == "paper" && form.ToLower() == "paper")
                {

                    Console.WriteLine("Draw. Try again.");
                    Console.WriteLine("Player: " + playerMove);
                    Console.WriteLine("Computer: " + form);
                    computerCount = computerCount + 0;
                    playerCount = playerCount + 0;
                    Console.WriteLine("Computer Score: " + computerCount);
                    Console.WriteLine("Player Score: " + playerCount);

                }
                else if (playerMove.ToLower() == "paper" && form.ToLower() == "scissors")
                {

                    Console.WriteLine("Computer Wins.");
                    Console.WriteLine("Player: " + playerMove);
                    Console.WriteLine("Computer: " + form);
                    computerCount = computerCount + 1;
                    playerCount = playerCount + 0;
                    Console.WriteLine("Computer Score: " + computerCount);
                    Console.WriteLine("Player Score: " + playerCount);

                }
                else if (playerMove.ToLower() == "scissors" && form.ToLower() == "rock")
                {

                    Console.WriteLine("Computer Wins.");
                    Console.WriteLine("Player: " + playerMove);
                    Console.WriteLine("Computer: " + form);
                    computerCount = computerCount + 1;
                    playerCount = playerCount + 0;
                    Console.WriteLine("Computer Score: " + computerCount);
                    Console.WriteLine("Player Score: " + playerCount);


                }
                else if (playerMove.ToLower() == "scissors" && form.ToLower() == "paper")
                {

                    Console.WriteLine("Player Wins.");
                    Console.WriteLine("Player: " + playerMove);
                    Console.WriteLine("Computer: " + form);
                    computerCount = computerCount + 0;
                    playerCount = playerCount + 1;
                    Console.WriteLine("Computer Score: " + computerCount);
                    Console.WriteLine("Player Score: " + playerCount);


                }
                else if (playerMove.ToLower() == "scissors" && form.ToLower() == "scissors")
                {

                    Console.WriteLine("Draw. Try Again.");
                    Console.WriteLine("Player: " + playerMove);
                    Console.WriteLine("Computer: " + form);
                    computerCount = computerCount + 0;
                    playerCount = playerCount + 0;
                    Console.WriteLine("Computer Score: " + computerCount);
                    Console.WriteLine("Player Score: " + playerCount);

                }
                else if (playerMove.ToLower() != "rock" || playerMove.ToLower() != "paper" || playerMove.ToLower() != "scissors")
                {

                    Console.WriteLine("Invalid input!");

                }

            }

        }
    }
}